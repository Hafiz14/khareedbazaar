﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KhareedBazaar.Web.Startup))]
namespace KhareedBazaar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
