﻿using KhareedBazaar.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhareedBazaar.Database
{
    public class CBContext: DbContext
    {
        public CBContext() : base("KhareedBazaarConnection")
        {

        }
           
        public DbSet<Product>Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
